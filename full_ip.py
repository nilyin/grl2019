#!/usr/bin/env python3
from threading import Thread
import numpy as np
from datetime import date, timedelta
import os


def main():
    for hour in ['00', '12']:
        ip_array = np.zeros((3,15,49))
        startday, endday = date(2015,12,31), date(2016,12,30)
        day_number = (endday - startday).days
        print(day_number)
        thread_number = 32
        day = startday
        for block in range(day_number // thread_number +1 ):
            threads = [None] * thread_number
            results = [None] * thread_number
            for  i in range(thread_number):
                threads[i] = Thread(target = day_ip, args=(day, hour, results, i))
                threads[i].start()
                day += timedelta(days=1)
            for  i in range(thread_number):
                threads[i].join() 
                ip_array += results[i] 
        np.save('FULL-IP-' + hour, ip_array/(day_number+1))


def day_ip(day,hour,results,index):
    print(day, hour)
    ip_result = np.zeros((3,15,49))
    if date(2015,12,31) <= day <= date(2016,12,30):
        ds = np.load('ds.npy')
        terr = np.load('terr_array.npy')
        datafile = os.path.join(os.getenv('PWD'),'data',day.strftime('DATA-%Y-%m-%d-' + hour + '.npz')) 

        lat,lon = 180,360
        dtime = 49
        h0 = 6000
        sigma0 = 6e-14
        j0 = 1e-9
        cape_0 = 1/3000     # invert cape_0
        coeff = j0 * h0 / sigma0

        rp2h = np.zeros((dtime,lat,lon))
        
        cape = np.load(datafile)['cape']
        ctop = np.load(datafile)['ctop']
        cbot = np.load(datafile)['cbot']
        rain = np.load(datafile)['rainc']
        prwa = np.load(datafile)['pw']

        rp2h[1:-1,:,:] = rain[2:,:,:] - rain[:-2,:,:]          # rain per 2 hours  

        for i in range(15):    
            ip_result[0,i,:] = (coeff * ds * terr[i] * (np.exp(-cbot/h0) - np.exp(-ctop/h0)) * cape * cape_0 * (rp2h / prwa) * np.heaviside(cape - 1000, 1)).sum(axis=(1,2))
            ip_result[1,i,:] = (ds * cape * terr[i]).sum(axis=(1,2))  
            ip_result[2,i,:] = (rp2h * terr[i]).sum(axis=(1,2))  

    results[index] = ip_result


if __name__ == '__main__':
    main()
