#!/usr/bin/env python3
import numpy as np
from datetime import date, timedelta
from dateutil.rrule import rrule, DAILY
import os


def fmin(array):
    result = (0.5 * array[18,:,:] + array[19:42,:,:].sum(axis=0) + 0.5 * array[42,:,:]) / 24
    return result


hours = ['00', '12']
path = os.path.join(os.getenv('PWD'), 'data')  #gdas

startday = date(2015,12,31)
endday = date(2016,12,30)
number = (1 + (endday-startday).days)*len(hours)
print(number)

lat,lon = 180,360
dtime = 49
    
mean_cape = np.zeros((lat,lon))

for s_hour in hours:    
    for day in rrule(DAILY, dtstart=startday, until=endday):
        print(day)
        day_h = day.strftime('-%Y-%m-%d-' + s_hour + '.npz')

        rp2h = np.zeros((dtime,lat,lon))

        cape = np.load(os.path.join(path, 'DATA' + day_h))['cape']

        mean_cape += fmin(cape) 


np.save('MEANCAPE', mean_cape/number)
